import React from 'react';
import ReactDOM from 'react-dom';

import Wheel from './wheel';

import './index.css';

export const App = () => {
  return (
    <div className="App">
      <Wheel port={ 3001 } />
    </div>
  );
}

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);