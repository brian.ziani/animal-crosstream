// inspired from https://codesandbox.io/s/github/hadriengerard/spinning-wheel-game-react
import React from 'react';

import './wheel.css';

export default class Wheel extends React.Component {
    constructor({ port }) {
        super();
        this.state = {
            selectedItem: null,
            illustrations: null,
            items: null,
        };
        
        this.ws = new WebSocket('ws://localhost:' + port);
    };

    componentDidMount = () => {
      this.ws.onopen = () => {
        this.ws.send(JSON.stringify({ type: 'read', path: 'wheel_illustrations' }));
        this.ws.send(JSON.stringify({ type: 'read', path: 'wheel_items' }));
        this.ws.send(JSON.stringify({ type: 'read', path: 'wheel_state' }));
      };

      this.ws.onmessage = (message) => {
        message = JSON.parse(message.data);
        if(message.type === 'read' || message.type === 'update') {
          if(message.path === 'wheel_illustrations')
            this.setState({ illustrations: message.data.map((illus) => { return { path: illus.path, color: illus.color } }) });
          if(message.path === 'wheel_items')
            this.setState({ items: message.data.map((item) => item.message) });
        }
        if(message.type === 'update' && message.path === 'wheel_state')
          if(message.data[0].state === 'resting')
            this.setState({ selectedItem: null });
          if(message.data[0].state === 'spinning')
            this.setState({ selectedItem: (this.state.items !== null) ? (Math.floor(Math.random() * this.state.items.length)) : 0 });
      };
    };

    render = () => {
        const { selectedItem, illustrations, items } = this.state;
    
        const wheelVars = {
          '--nb-item': (items !== null) ? items.length : 5,
          '--selected-item': (selectedItem !== null) ? selectedItem : 0,
        };
        const spinning = (selectedItem !== null) ? 'spinning' : '';
    
        return (
          <div className="wheel-container">
            <img src="/wheel/arrow.svg" id="arrow-wheel" alt="Arrow wheel" style={{ '--wheel-arrow-aspect-ratio': 0.481}} />
            <div className={`wheel ${spinning}`} style={wheelVars}>
              {
                (items !== null)
                ? items.map((item, index) => (
                    <div
                      key={index}
                      className="wheel-item"
                      style={{
                        '--item-nb': index,
                        '--bg-color-wheel-item': (illustrations !== null)
                        ? illustrations[index % illustrations.length].color
                        : '#999'
                      }}
                    >
                      <p>{item}</p>
                      <img
                        className="wheel-illustration"
                        src={ (illustrations !== null)
                              ? illustrations[index % illustrations.length].path
                              : "/wheel/no_image_available.svg"
                            }
                        alt="Wheel element"
                      />
                    </div>
                ))
                : (
                <div className="no-data-available">
                  <p>No data available</p>
                </div>
                )
              }
            </div>
          </div>
        );
      };
}