import React from 'react';
import '../../App.css';

export default class QuestionForm extends React.Component {
    constructor({ collection, port }) {
        super();
        this.state = {
            port: port,
            collection: collection,
            input: '',
            checked: true
        };

        this.ws = new WebSocket('ws://localhost:' + port);

        this.submitData = this.submitData.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleChecked = this.handleChecked.bind(this);
    };

    componentDidMount = () => {
        this.ws.onopen = () => this.ws.send(JSON.stringify({ type: 'read', path: this.state.collection }));
    };

    submitData = (event) => {
        event.preventDefault();
        if(this.state.input !== '') {
            this.ws.send(JSON.stringify({ type: 'update', path: this.state.collection, id: 1, data: { message: this.state.input } }));
            this.setState({ input: '' });
        }
    };

    handleOnChange = (event) => {this.setState({ input: event.target.value })};
    handleChecked = () => {
        this.setState({checked: !this.state.checked},
            () =>  this.ws.send(JSON.stringify({ type: 'update', path: this.state.collection, id: 1, data: { display: this.state.checked } }))
        );
    };

    render = () => {
        return (
            <div className={'wrapper'}>
                <form onSubmit={this.submitData} className={'data-form'}>
                    <input
                        className={'input-text'}
                        value={this.state.input}
                        onChange={event => this.handleOnChange(event)}
                        type='text'
                        placeholder='Enter Question'
                        required
                    ></input>
                    <input className={'btn-primary input-submit'} type='submit' value='Ask'></input>
                </form>
                <form className={'data-form'}>
                    <label>Display the Question</label>
                    <input
                         type="checkbox"
                         checked={this.state.checked} 
                         onChange={this.handleChecked}
                    ></input>
                </form>
            </div>
        )
    };
};