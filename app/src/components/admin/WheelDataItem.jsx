import React from 'react';
import '../../App.css';

export default class WheelDataItem extends React.Component {
    constructor({ collection, port }) {
        super();
        this.state = {
            collection: collection,
            data: null,
        };

        this.ws = new WebSocket('ws://localhost:' + port);
        this.deleteData = this.deleteData.bind(this);
    };

    componentDidMount = () => {
        this.ws.onopen = () => {
            this.ws.send(JSON.stringify({ type: 'read', path: this.state.collection }));
        };

        this.ws.onmessage = (message) => {
            message = JSON.parse(message.data);
            if(message.type === 'read' || message.type === 'update')
                if(message.path === this.state.collection)
                    this.setState({ data: message.data });
        };
    };

    deleteData = (id) => this.ws.send(JSON.stringify( { type: 'delete', path: this.state.collection, id: id } ));

    render = () => {
        return (
            <table>
                <thead>
                    <tr>
                        <th>Activities</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                {
                    (this.state.data !== null) ? this.state.data.map((data, id) => 
                        <tr key={id}>
                            <td>{data.message}</td>
                            <td><button className={'btn-danger'} onClick={() => this.deleteData(data.id)}>Delete</button></td>
                        </tr>
                        )
                    : <tr><td>No Data</td></tr>
                }
                </tbody>
            </table>
        )
    };
};
