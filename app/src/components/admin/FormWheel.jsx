import React from 'react';
import WheelDataItem from './WheelDataItem'
import '../../App.css';

export default class FormWheel extends React.Component {
    constructor({ collection, port }) {
        super();
        this.state = {
            port: port,
            collection: collection,
            input: ''
        };

        this.ws = new WebSocket('ws://localhost:' + port);
                
        this.submitData = this.submitData.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
    };

    componentDidMount = () => {
        this.ws.onopen = () => {
            this.ws.send(JSON.stringify({ type: 'read', path: this.state.collection }));
        }
    };

    submitData = (event) => {
        event.preventDefault();
        if(this.state.input !== '') {
            this.ws.send(JSON.stringify({ type: 'create', path: this.state.collection, data: { message: this.state.input } }));
            this.setState({ input: '' });
        }
    };

    handleOnChange = (event) => this.setState({ input: event.target.value });

    render = () => {
        return (
            <div className={'wrapper'}>
                <WheelDataItem collection={this.state.collection} port={this.state.port} />
                <form onSubmit={this.submitData} className={'data-form'}>
                    <input
                        className={'input-text'}
                        value={this.state.input}
                        onChange={event => this.handleOnChange(event)}
                        type='text'
                        placeholder='Enter Activity'
                        required
                    ></input>
                    <input className={'btn-primary input-submit'} type='submit' value='Send Activity'></input>
                </form>
            </div>
        )
    };
};

