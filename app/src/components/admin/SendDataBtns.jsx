import React from 'react';
import '../../App.css';

export default class Buttons extends React.Component {
    constructor({ actions, username, collection, port }) {
        super();
        this.state = {
            actions: actions,
            username: username,
            collection: collection,
            data: null,
        };

        this.ws = new WebSocket('ws://localhost:' + port);

        this.sendData = this.sendData.bind(this);
    };

    componentDidMount = () => {
        this.ws.onopen = () => this.ws.send(JSON.stringify({type: 'read', path: this.state.collection }));
    }

    sendData = (type, label) => {
        this.setState({
            data: {
                id: 1,
                type: type,
                label: label
            }
        }, () => {
            this.ws.send(JSON.stringify({
                type: 'update',
                path: this.state.collection,
                id: 1,
                data: this.state.data
            }))
        });
    };

    render = () => {
        return (
            <div>
                {
                    this.state.actions === 'don'
                        ? <button className={'btn-primary'} onClick={() => {
                            this.sendData(this.state.actions, this.state.username + " has donated " + Math.floor(Math.random() * 50) + "$ !")
                        }}>Donate</button>
                        : <div></div>
                }
                {
                    this.state.actions === 'sub'
                        ? <button className={'btn-primary'} onClick={() => {
                            this.sendData(this.state.actions, this.state.username + " is wonderful 💖")
                        }}>Subscribe</button>
                        : <div></div>
                }
                {
                    this.state.actions === 'follow'
                        ? <button className={'btn-primary'} onClick={() => {
                            this.sendData(this.state.actions, this.state.username + " joined the community !")
                        }}>Follow</button>
                        : <div></div>
                }
                {
                    this.state.actions === 'prime'
                        ? <button className={'btn-primary'} onClick={() => {
                            this.sendData(this.state.actions, "Thanks for your prime " + this.state.username + " !")
                        }}>Prime</button>
                        : <div></div>
                }
                {
                    this.state.actions === 'success-viewer'
                        ? <button className={'btn-primary'} onClick={() => {
                            this.sendData("viewer", "Passed " + Math.floor(Math.random() * 10000) + " viewers")
                        }}>Success viewers</button>
                        : <div></div>
                }
                {
                    this.state.actions === 'success-follow-2'
                        ? <button className={'btn-primary'} onClick={() => {
                            this.sendData("follow-2", "Thanks to " + Math.floor(Math.random() * 10000) + " followers")
                        }}>Success follow 2</button>
                        : <div></div>
                }
                {
                    this.state.actions === 'success-follow'
                        ? <button className={'btn-primary'} onClick={() => {
                            this.sendData("follow", "Reached " + Math.floor(Math.random() * 10000) + " followers")
                        }}>Success follow</button>
                        : <div></div>
                }
            </div>
        )
    };
};