import React from 'react';
import Buttons from './SendDataBtns';
import FormWheel from './FormWheel';
import SpinWheelBtn from './SpinWheelBtn';
import QuestionForm from './QuestionForm';
import '../../App.css';

export default class Admin extends React.Component {

    constructor() {
        super();
        this.state = {
            collection: 'users',
            data: null,
        };

        this.ws = new WebSocket('ws://localhost:3001');
    };

    componentDidMount = () => {
        document.title = 'Animal Crosstream - ADMIN PANEL';
        this.ws.onopen = () => {
            this.ws.send(JSON.stringify({ type: 'read', path: this.state.collection}));
        };
        
        this.ws.onmessage = (message) => {
            message = JSON.parse(message.data);
            this.setState({ data: message.data[Math.floor(Math.random() * message.data.length)] });
        };
    };
    

    render = () => {
        return  (
            <div className={'container'}>
                <h1 className={'title-main'}>Control Panel</h1>
                    {
                    (this.state.data !== null)
                    ?
                    <div className={'row'}>
                        <Buttons key={'don'} actions={'don'} username={this.state.data.username} collection={'alerts'} port={3001}></Buttons>
                        <Buttons key={"sub"} actions={"sub"} username={this.state.data.username} collection={'alerts'} port={3001}></Buttons>
                        <Buttons key={"follow"} actions={"follow"} username={this.state.data.username} collection={'alerts'} port={3001}></Buttons>
                        <Buttons key={"prime"} actions={"prime"} username={this.state.data.username} collection={'alerts'} port={3001}></Buttons>
                        <SpinWheelBtn key={"spin_wheel"} port={3001} collection={'wheel_state'} ></SpinWheelBtn>
                        <Buttons key={"success_viewer"} actions={"success-viewer"} collection={'successes'} port={3001}></Buttons>
                        <Buttons key={"success_follow"} actions={"success-follow"} collection={'successes'} port={3001}></Buttons>
                        <Buttons key={"success_follow_2"} actions={"success-follow-2"} collection={'successes'} port={3001}></Buttons>
                    </div>
                    :
                    <div className={'row'}>Impossible to render button because lack of data</div>
                    }
                <h1 className={'title-main'}>Wheel Data</h1>
                    <FormWheel collection={ 'wheel_items' } port={ 3001 } />
                <h1 className={'title-main'}>Questions Parameter</h1>
                    <QuestionForm collection={'questions'} port={ 3001 }></QuestionForm>
            </div>
        )
    };
};
