import React from 'react';
import '../../App.css';

export default class SpinWheelBtn extends React.Component {
    constructor({ collection, port }) {
        super();
        this.state = {
            collection: collection,
            label: 'Spin the wheel',
            wheel_state: 'resting',
        };
        this.setWheelState = this.setWheelState.bind(this);
        this.ws = new WebSocket('ws://localhost:' + port);
    };

    componentDidMount = () => {
        this.ws.onopen = () => {
            this.ws.send(JSON.stringify({ type: 'read', path: this.state.collection }));
            this.setWheelState(this.state.wheel_state);
        }
    };

    setWheelState = (new_state) => {
        this.setState({ wheel_state: new_state });
        this.ws.send(JSON.stringify({ type: 'update', path: this.state.collection, id: 1, data: { state: new_state } }));
    };

    render = () => {
        return (
            <div>
                <button
                    className={'btn-primary'}
                    onClick={(this.state.wheel_state === 'resting') ? () => this.setWheelState('spinning') : () => this.setWheelState('resting')}
                >
                { (this.state.wheel_state === 'resting') ? 'Spin the wheel' : 'Reset the wheel' }
                </button>
            </div>
        )
    };
};