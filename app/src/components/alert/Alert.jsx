import React from "react";
import './Alert.css';

class Alert extends React.Component {

    constructor(props) {
        super(props);
        this.wrapperRef = React.createRef();

        /**
         * @state.label string
         * @state.type { "follow", "don", "prime", "sub"}
         */

        this.state = {
            type: "don",
            label: "Je suis un texte par défaut",
            show: false
        };
        this.ws = new WebSocket('ws://localhost:3001');
    };

    componentDidMount() {
        this.ws.onopen = () => this.ws.send(JSON.stringify({type: 'read', path: 'alerts'}));

        this.ws.onmessage = (message) => {
            message = JSON.parse(message.data);
            if ((message.type === 'read' || message.type === 'update') && Array.isArray(message.data)) {
                this.setState({show: true, type: message.data[0].type, label: message.data[0].label});
                setTimeout(() => this.closeCurrentNotification(), 3000);
            }
            if (message.type === 'error') {
                console.log("ERROR SOCKET: " + message.data);
            }
        };
    };

    closeCurrentNotification = () => this.setState({show: false});

    render() {

        const fadeInClassName = (this.state.show) ? "fade-in" : "";
        const alertType = (this.state.show) ? "alert-" + this.state.type : "";

        return (
            <div ref={this.wrapperRef} className={"alert " + alertType + " " + fadeInClassName}>
                <span className="alert-label">{this.state.label}</span>
                <div className="alert-badge">&nbsp;</div>
            </div>
        )
    };
}

export default Alert