import React from "react";
import './Success.css'

class Success extends React.Component {

    constructor(props) {
        super(props);
        this.wrapperRef = React.createRef();

        /**
         * @state.label string
         * @state.type { "follow", "follow-2", "viewer"}
         */
        this.state = {
            type: "viewer",
            label: "Je suis un texte par défaut",
            show: false
        };
        this.ws = new WebSocket('ws://localhost:3001');
    };

    componentDidMount() {
        this.ws.onopen = () => this.ws.send(JSON.stringify({type: 'read', path: 'successes'}));

        this.ws.onmessage = (message) => {
            message = JSON.parse(message.data);
            if ((message.type === 'read' || message.type === 'update') && Array.isArray(message.data)) {
                this.setState({show: true, type: message.data[0].type, label: message.data[0].label});
                setTimeout(() => this.closeCurrentNotification(), 3000);
            }
            if (message.type === 'error') {
                console.log("ERROR SOCKET: " + message.data);
            }
        };
    };

    closeCurrentNotification = () => this.setState({show: false});

    render() {

        const fadeInClassName = (this.state.show) ? "fade-in" : "";
        const successType = (this.state.show) ? "success-" + this.state.type : "";

        return (
            <div ref={this.wrapperRef}
                 className={"success " + successType + " " + fadeInClassName}>
                <span className="success-label">{this.state.label}</span>
                <div className="success-badge">&nbsp;</div>
            </div>
        )
    };
}

export default Success