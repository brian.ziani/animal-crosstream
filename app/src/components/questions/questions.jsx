import React from "react";
import './questions.css'

class Questions extends React.Component {

    constructor({ port }) {
        super();
        this.wrapperRef = React.createRef();

        this.state = {
            question: "",
            show: true,
        };

        this.ws = new WebSocket('ws://localhost:3001');
    };

    componentDidMount() {
        this.ws.onopen = () => this.ws.send(JSON.stringify({type: 'read', path: 'questions'}));

        this.ws.onmessage = (message) => {
            message = JSON.parse(message.data);
            if((message.type === 'read' || message.type === 'update') && message.data[0].display===true ){
                this.setState({question: message.data[0].message, show: true});
            }
            if((message.type === 'read' || message.type === 'update') && message.data[0].display===false ){
                this.setState({question: message.data[0].message, show: false});
            }
        };
    };

    render() {

        const fadeInClassName = (this.state.show) ? "fade-in" : "";
        
        return (
            <div ref={this.wrapperRef} className={"questions" + fadeInClassName}>
                <span className="questions-label">{this.state.question}</span>
                <div className="questions-bulle">&nbsp;</div>
                <div className="questions-badge">&nbsp;</div>
                <div className="questions-interrogation-mark">&nbsp;</div>
            </div>
        )
    };

};

export default Questions
