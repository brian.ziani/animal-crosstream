import React, {Suspense} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Loading from '../components/utils';

const Wheel = React.lazy(() => import('../components/wheel/'));
const Questions = React.lazy(() => import('../components/questions/questions'));
const Admin = React.lazy(() => import('../components/admin/'));
const Success = React.lazy(() => import('../components/success/Success'));
const Alert = React.lazy(() => import('../components/alert/Alert'));

const Routes = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <Router>
                <Switch>
                    <Route exact path='/' render={() => <Admin/>}/>
                    <Route exact path='/wheel' render={() => <Wheel/>}/>
                    <Route exact path='/questions' render={() => <Questions/>}/>
                    <Route exact path='/success' render={() => <Success/>}/>
                    <Route exact path='/alert' render={() => <Alert/>}/>
                    <Route exact path='/admin' render={() => <Admin/>}/>
                </Switch>
            </Router>
        </Suspense>
    );
};

export default Routes
